/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo2;

/**
 *
 * @author andsa
 */
public class Pagos {
    private int numDocente;
    private String nombre; 
    private String domicilio;
    private float nivel;
    private int pagoHora;
    private int horasImp;
    
    //Constructores
    public Pagos(){
        this.domicilio="";
        this.horasImp=0;
        this.nivel=0.0f;
        this.nombre="";
        this.numDocente=0;
        this.pagoHora=0;
    }
    
    public Pagos(int numDocente, String nombre, String domicilio, float nivel, int pagoHora, int horasImp){
        this.domicilio=domicilio;
        this.horasImp=horasImp;
        this.nivel=nivel;
        this.nombre=nombre;
        this.numDocente=numDocente;
        this.pagoHora=pagoHora;
    }
    
    public Pagos(Pagos otro){
        this.domicilio=otro.domicilio;
        this.horasImp=otro.horasImp;
        this.nivel=otro.nivel;
        this.nombre=otro.nombre;
        this.numDocente=otro.numDocente;
        this.pagoHora=otro.pagoHora;
    }
    
    //Setter & Getter

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getNivel() {
        return nivel;
    }

    public void setNivel(float nivel) {
        this.nivel = nivel;
    }

    public int getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(int pagoHora) {
        this.pagoHora = pagoHora;
    }

    public int getHorasImp() {
        return horasImp;
    }

    public void setHorasImp(int horasImp) {
        this.horasImp = horasImp;
    }
    
    //Métodos de comportamiento
    public float calcularPagoHoras(){
        float pagoHoras=this.pagoHora*this.horasImp*this.nivel;
        return pagoHoras;
    }
    
    public float calcularBono(){
        int hijos=0;
        float bono=0.0f;
         if (hijos>=1 && hijos <= 2) {
            bono=this.calcularPagoHoras()* 0.05f;
        } else if (hijos >= 3 && hijos <= 4) {
            bono=this.calcularPagoHoras()*0.10f;
        } else if (hijos > 5) {
            bono=this.calcularPagoHoras()*0.20f;
            
        } else if (hijos==0){
            bono=0;
        }
        return bono;
    }
    
     public float calcularImpuesto(){
        float impuesto=0.0f;
        impuesto=this.calcularPagoHoras()*0.16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float totalPagar=0.0f;
        totalPagar=this.calcularPagoHoras()-this.calcularImpuesto();
        return totalPagar;
    }
    
   
}